
#-quicklisp
(let ((quicklisp-init #P"/opt/orbi/quicklisp/setup.lisp"))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))


(ql:quickload '(parenscript paren6 rx html-entities
                            spinneret spinneret/ps
                            clack woo optima alexandria uiop))

(asdf:load-asd
 (merge-pathnames #p"projects/ps-react-example/ps-react-example.asd"
                  (user-homedir-pathname)))

(asdf:load-system :ps-react-example)
#|
(in-package #:cl-user)

(load
 (merge-pathnames #p"projects/parenscript-react-examples-17-0.2/psx.lisp"
                  (user-homedir-pathname)))
|#

;;(in-package #:react-examples/spa)

(defparameter *debug* t)

(ps-react-example:clack-start)

