
(in-package #:ps-react-example)

(rx:defm welcome-fn ()
  `(defun -welcome (props)
     (ps:if (eql undefined props)
            (rx:react-element -alert
                              (rx:{} variant "warning")
                              (alert-v "Welcome 'unknown name'"))
            (let ((tag- (rx:get-prop props 'tag)))
              (ps:when (ps:eql "undefined" tag-) (ps:setf tag- "h4"))
              (rx:react-element tag-
                                (rx:{} key (ps:@ props name))
                                "Hello " (ps:@ props name))))))

(rx:defm greetings-fn ()
  `(defun -greetings (props)
     (let ((tag- (rx:get-prop props 'tag)))
       (ps:when (ps:eql undefined tag-) (ps:setf tag- "h4"))
       (if (rx:get-prop props 'hidden)
           (rx:react-element :h1)
           (rx:react-element :div
                             (rx:{} id "greeting")
                             (rx:ps-map ("Sara" "Cahal" "Edita")
                                        (lambda (n)
                                          (-welcome (rx:{} key n
                                                               name n
                                                               tag tag-)))))))))

(rx:defm greeting-tab ()
  `(rx:react-bootstrap-tab* -tab
                            (rx:{} event-key "greeting" title "Greeting")
                            (rx:react-element -greetings
                                              (rx:{} tag "h2"
                                                         hidden false))))

(rx:defm render-greetings (tag)
  `(rx:react-dom-render (rx:react-element -greetings (rx:{} tag "h2"))
                        (rx:doc-element ,tag)))

