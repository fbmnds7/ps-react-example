
(in-package #:ps-react-example)


(rx:defm home-tab ()
  `(rx:react-bootstrap-tab* -tab
                            (rx:{} event-key "home" title "Home")
                            ,*sonnet-58*))

(rx:defm profile-tab ()
  `(rx:react-bootstrap-tab* -tab
                            (rx:{} event-key "profile" title "Profile")
                            ,*sonnet-86*))

(rx:defm contact-tab ()
  `(rx:react-bootstrap-tab* -tab
                            (rx:{} event-key "contact"
                                   title "Contact"
                                   disabled t)
                            "contact"))

(rx:defm tabs (tag)
  `(progn
     (rx:react-dom-render
      (rx:react-bootstrap-tab* -tabs
                               (rx:{} default-active-key "profile"
                                      id "uncontrolled-tab-example"
                                      class-name "mb-3")
                               (home-tab)
                               (profile-tab)
                               (greeting-tab)
                               (less-text-tab)
                               (contact-tab))
      (rx:doc-element ,tag))))

