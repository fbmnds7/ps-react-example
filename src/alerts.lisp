
(in-package #:ps-react-example)

(defparameter *alert-categories*
  (list "primary"
        "success"
        "danger"
        "warning"
        "info"
        "light"
        "dark"))

(rx:defm alert-v (v)
  `(#'(lambda (x) (+ "Alert " x)) ,v))

(rx:defm alert-fn ()
  `(defun -alert (props)
     (rx:react-element (ps:@ -react-bootstrap -alert)
                       props
                       (alert-v (ps:@ props variant)))))

(defmacro alert-list ()
  `(loop
     for v in *alert-categories*
     collect `(rx:react-element -alert
                                (rx:{} variant ,v key ,v))))

(rx:defm alert-list-fn ()
  `(defun -alert-list ()
     (rx:react-element :div nil ,@(ps:lisp (alert-list)))))

(rx:defm render-alerts ()
  `(rx:react-dom-render (rx:react-element -alert-list)
                        (rx:doc-element "alerts")))


