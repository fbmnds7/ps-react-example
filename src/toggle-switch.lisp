
(in-package :ps-react-example)

(defparameter *relays* '("relay 1"
                         "relay 2"
                         "relay 3"
                         "relay 4"))

(rx:defm render-toggle-switch (tag props)
  `(rx:react-dom-render (rx:react-element -toggle-switch ,props)
                        (rx:doc-element ,tag)))

(rx:defm toggle-relay-fn (url)
  `(rx:js (format nil "async function toggleRelay2 () { 
    try {
        const response = await fetch('~a', {
        mode: 'no-cors',
        cache: 'no-cache'
        }); 
        return setRelay1(!relay1); 
    } catch (e) { return null; }
}" ,url)))

(rx:defm relay-switch-fn (state id label)
  (let ((set-state% (format nil "set~a(!~a)"
                            (string-capitalize state) state)))
    `(defun -relay-switch ()
       (rx:use-state ,state 'false)
       (toggle-relay-fn "http://192.168.178.37/r1")
       (let ((props (rx:{} id ,id
                           html-for ,id
                           label ,label
                           checked relay1
                           on-change toggle-relay2)))
         (rx:react-element :div nil
                           (rx:react-element -toggle-switch props)
                           (rx:react-element :label
                                             (rx:{} html-for ,id)
                                             ,label))))))

(rx:defm render-relay-switch (tag)
  `(rx:react-dom-render (rx:react-element -relay-switch)
                        (rx:doc-element ,tag)))




