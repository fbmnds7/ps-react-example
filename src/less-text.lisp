
(in-package #:ps-react-example)


(rx:defm -less-text-fn ()
  `(defun -less-text (props)
     (rx:use-state "hidden" 'true)
     (rx:react-element :span nil
                       (if (ps:@ props hidden) "trimmed" (ps:@ props text))))
#|
     (ps:if (ps:<= (ps:chain (ps:@ props text) length)
                   (ps:@ props max-length))
            (set-hidden (ps:not nil))
            (set-hidden nil))
|#
  )

(rx:defm fn-less-text (props)
  (cond ((listp props)
         (let* ((p (string-trim '(#\;) (ps:ps* `,props)))
                (js (format nil "LessText(~a)" p)))
           `(rx:js ,js)))
        (t (error "unexpected argument in -less-text"))))

(defparameter *less-text-expanded*
  "Focused, hard work is the real key to success. Keep your eyes on the goal, and just keep taking the next step towards completing it.")

(rx:defm less-text-tab ()
  `(rx:react-bootstrap-tab* -tab
                            (rx:{} event-key "lesstext" title "Less Text")
                            (rx:react-element :div
                                              (rx:{} id "lesstext")
                                              "")))

(rx:defm -less-text (tag)
  `(rx:react-dom-render
    (let ((props `,(rx:{} hidden false
                          text ,*less-text-expanded*)))
      (rx:react-element -less-text props))
    (rx:doc-element ,tag)))

