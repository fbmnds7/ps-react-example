
(in-package #:ps-react-example)


(rx:defm buttons-fn ()
  `(defun -buttons ()
     (rx:use-state "greetings" 'false)
     (defun handle-submit (e)
       ((ps:@ e prevent-default))
       (when (eql (ps:@ e native-event submitter id) "primary")
         (set-greetings (not greetings)))
       ((ps:@ console log) e))
     (rx:react-element :form (rx:{} class-name "d-grid gap-2"
                                    on-submit #'handle-submit)
                       (rx:react-element :button
                                         (rx:{} class-name
                                                "btn btn-primary btn-lg"
                                                id "primary")
                                         "Block Level Primary Button")
                       (rx:react-element :button
                                         (rx:{} class-name
                                                "btn btn-secondary btn-lg"
                                                id "secondary")
                                         "Block Level Secondary Button")
                       (rx:react-element -Greetings
                                         (rx:{} class-name
                                                "btn-greetings"
                                                hidden greetings)))))

(rx:defm render-buttons (tag)
  `(rx:react-dom-render (rx:react-element -buttons nil)
                        (rx:doc-element ,tag)))


