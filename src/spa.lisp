
(in-package #:ps-react-example)


#+null
(ps:ps
  (ps6:defclass6 (-hello (chain -react -component))
      (defstatic render ()
        (psx::htm (:div "Hello World")))))

(defparameter *app-js*
  (ps:ps*
   `(progn
      (alert-fn)
      (alert-list-fn)
      (buttons-fn)
      (render-buttons "buttons")
      (rx:toggle-switch-fn)
      (relay-switch-fn "relay1" "switch" "Relay 1")
      (render-relay-switch "relay 1")
      (welcome-fn)
      (greetings-fn)
      (render-alerts)
      (tabs "tabs")
      (-less-text-fn)
      (-less-text "lesstext")
      (three "three"))))

(defparameter *index*
  (sp:with-html-string
      (:doctype)
      (:html
       (:head
        (:title "Hello React")
        (:link :rel "stylesheet" :href "/css/bootstrap.css")
        (:link :rel "stylesheet" :href "/css/toggle-switch.css")
        (:link :rel "icon" :href "/assets/favicon.ico")
        (:script :type "application/javascript" :src "/js/bootstrap-bundle.js")
        (:script :type "application/javascript" :src "/js/react.js")
        (:script :type "application/javascript" :src "/js/react-dom.js")
        (:script :type "application/javascript" :src "/js/react-bootstrap.js")
        (:script :type "application/javascript" :src "/js/three.js"))
       (:body
        (:div :id "buttons")
        (:div :id "welcome")
        (:div :id "alerts")
        (:div :id "three")
        (:div :id "relay 1")
        (:div :id "relay 2")
        (:div :id "relay 3")
        (:div :id "relay 4")
        (:div :id "tabs")
        (:script :type "application/javascript" :src "/js/App.js")))))

(defun handler (env)
  (let ((js-hdr '(:content-type "application/javascript"))
        (path (getf env :path-info)))
    (handler-case
        (or
         (rx:route path "/index.html" 200 nil *index*)
         (rx:route path "/js/react.js" 200 js-hdr *react*)
         (rx:route path "/js/react-dom.js" 200 js-hdr *react-dom*)
         (rx:route path "/js/react-bootstrap.js" 200 js-hdr *react-bootstrap*)
         (rx:route path "/js/three.js" 200 js-hdr *three*)
         (rx:route path "/js/App.js" 200 js-hdr *app-js*)
         (rx:route path "/css/toggle-switch.css" 200 nil *toggle-switch-css* t)
         (rx:route path "/css/bootstrap.css" 200 nil *bootstrap-css* t)
         (rx:route path "/js/bootstrap-bundle.js" 200 js-hdr *bootstrap-bundle-js*)
         (rx:route path "/assets/favicon.ico"
                   200 '(:content-type "image/x-icon") *favicon* t)
         `(404 nil (,(format nil "Path not found~%"))))
      (t (e) (if *debug*
                 `(500 nil (,(format nil "Internal Server Error~%~A~%" e)))
                 `(500 nil (,(format nil "Internal Server Error"))))))))



