
(in-package #:ps-react-example)

(defparameter *sonnet-58*
  "That god forbid, that made me first your slave, I should in thought control your times of pleasure, Or at your hand the account of hours to crave, Being your vassal, bound to stay your leisure! O! let me suffer, being at your beck, The imprison'd absence of your liberty; And patience, tame to sufferance, bide each check, Without accusing you of injury. Be where you list, your charter is so strong That you yourself may privilage your time")

(defparameter *sonnet-86*
  "Was it the proud full sail of his great verse, Bound for the prize of all too precious you, That did my ripe thoughts in my brain inhearse, Making their tomb the womb wherein they grew? Was it his spirit, by spirits taught to write, Above a mortal pitch, that struck me dead? No, neither he, nor his compeers by night Giving him aid, my verse astonished. He, nor that affable familiar ghost Which nightly gulls him with intelligence,")

(defparameter *less-text-expanded*
  "Focused, hard work is the real key to success. Keep your eyes on the goal, and just keep taking the next step towards completing it.")


