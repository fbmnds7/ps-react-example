// First: import useState. It's a named export from 'react'
// Or we could skip this step, and write React.useState
//import React, { useState } from 'react';
//import ReactDOM from 'react-dom';

// This component expects 2 props:
//   text - the text to display
//   maxLength - how many characters to show before "read more"
function LessText({ text, maxLength }) {
  // Create a piece of state, and initialize it to `true`
  // `hidden` will hold the current value of the state,
  // and `setHidden` will let us change it
  const [hidden, setHidden] = React.useState(true);

  // If the text is short enough, just render it
  if (text.length <= maxLength) {
    React.Element( /*#__PURE__*/React.createElement("span", null, text));
  }

  // Render the text (shortened or full-length) followed by
  // a link to expand/collapse it.
  // When a link is clicked, update the value of `hidden`,
  // which will trigger a re-render
  React.Element( /*#__PURE__*/
  React.createElement("span", null,
  hidden ? `${text.substr(0, maxLength).trim()} ...` : text,
  hidden ? /*#__PURE__*/
  React.createElement("a", { onClick: () => setHidden(false) }, " read more") : /*#__PURE__*/

  React.createElement("a", { onClick: () => setHidden(true) }, " read less")));



}

ReactDOM.render( /*#__PURE__*/
React.createElement(LessText, {
  text: `Focused, hard work is the real key
      to success. Keep your eyes on the goal, 
      and just keep taking the next step 
      towards completing it.`,
  maxLength: 35 }),

document.querySelector('#lesstext'));


//export default LessText
