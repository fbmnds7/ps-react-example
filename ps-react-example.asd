
(asdf:defsystem #:ps-react-example
  :description "Describe ps-react-example here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on
  (#:asdf
   #:quicklisp
   #:parenscript
   #:paren6
   #:html-entities
   #:spinneret
   #:spinneret/ps
   #:rx
   #:clack
   #:woo
   #:optima
   #:alexandria
   #:uiop)
  :components
  ((:file "packages")
   (:file "psx")
   (:module "src"
    :components
    ((:file "env")
     (:file "assets")
     (:file "alerts")
     (:file "greetings")
     (:file "buttons")
     (:file "toggle-switch")
     (:file "tabs")
     (:file "less-text")
     (:file "three")
     (:file "spa")))
#|
   (:module "t"
    :components
    ((:file "load")))
|#
   ))

